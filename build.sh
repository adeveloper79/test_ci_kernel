#!/bin/bash
set -xe

OUT="$(realpath "$1" 2>/dev/null || echo 'out')"
mkdir -p "$OUT"

TMP=$(mktemp -d)
HERE=$(pwd)
SCRIPT="$(dirname "$(realpath "$0")")"/build

mkdir "${TMP}/system"
mkdir "${TMP}/partitions"

source "${HERE}/deviceinfo"

case $deviceinfo_arch in
    "armhf") RAMDISK_ARCH="armhf";;
    "aarch64") RAMDISK_ARCH="arm64";;
    "x86") RAMDISK_ARCH="i386";;
esac

mkdir image
TMPDOWN=$(mktemp -d)
cd "$TMPDOWN"
    git clone https://android.googlesource.com/platform/prebuilts/gcc/linux-x86/aarch64/aarch64-linux-android-4.9 -b pie-gsi --depth 1
    GCC_PATH="$TMPDOWN/aarch64-linux-android-4.9"
    if [ -n "$deviceinfo_kernel_clang_compile" ] && $deviceinfo_kernel_clang_compile; then
        git clone https://github.com/crdroidandroid/android_prebuilts_clang_host_linux-x86_clang-6875598 -b 10.0 --depth=1 linux-x86
        CLANG_PATH="$TMPDOWN/linux-x86"
    fi
    git clone https://android.googlesource.com/platform/prebuilts/gcc/linux-x86/arm/arm-linux-androideabi-4.9 -b pie-gsi --depth 1
    GCC_ARM32_PATH="$TMPDOWN/arm-linux-androideabi-4.9"
    
    git clone "$deviceinfo_kernel_source" --depth 1

    if [ -n "$deviceinfo_kernel_apply_overlay" ] && $deviceinfo_kernel_apply_overlay; then
        git clone https://android.googlesource.com/platform/system/libufdt -b pie-gsi --depth 1
        git clone https://android.googlesource.com/platform/external/dtc -b pie-gsi --depth 1
    fi
    ls .
cd "$HERE"

PATH="$GCC_PATH/bin:$GCC_ARM32_PATH/bin:${PATH}" \
"$SCRIPT/build-kernel.sh" "${TMPDOWN}" "${TMP}/system"

cp ${TMPDOWN}/KERNEL_OBJ/arch/arm64/boot/Image image/Image
cp ${TMPDOWN}/KERNEL_OBJ/arch/arm64/boot/Image /Image
cp ${TMPDOWN}/KERNEL_OBJ/arch/arm64/boot/Image.gz-dtb image/Image.gz-dtb
cp ${TMPDOWN}/KERNEL_OBJ/arch/arm64/boot/Image.gz-dtb /Image.gz-dtb
cp ${TMPDOWN}/KERNEL_OBJ/arch/arm64/boot/dts/qcom/sdm845-v2.dtb image/sdm845-v2.dtb

#rm -r "${TMP}"
#rm -r "${TMPDOWN}"

echo "done"
